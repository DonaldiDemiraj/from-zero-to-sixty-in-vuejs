import axios from 'axios';

axios.defaults.baseURL = 'https://jsonplaceholder.typicode.com';

export default class PostService {
    getAllPosts() {
        return axios.get('/posts');
    }

    getPost(number) {
        return axios.get(`/posts/${number}`);
    }

    writePost(post) {
        if (post.id) {
            return axios.put(`/posts/${post.id}`, post)
        }
        else
            return axios.post('/posts', post)

    }

    deletePost(id) {
        return axios.delete(`/posts/${id}`)
    }
}